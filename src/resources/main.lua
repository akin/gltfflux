
modelUtil = ModelUtil:new()
model = Model:new()

if modelUtil:load(model, "resources/Cube/Cube.gltf") then
	imageUtil = ImageUtil:new(model, "temp")
	print('Image count ' .. modelUtil:getImageCount(model))
	for i=0, modelUtil:getImageCount(model) - 1 do
		print('Compress ' .. i)
		imageUtil:compress(modelUtil:getImage(model, i))
	end

	if modelUtil:save(model, "resources/test.gltf") then
		print("Saving success")
	end
end