#ifndef GLTFFLUX_IMAGEUTIL_H_
#define GLTFFLUX_IMAGEUTIL_H_

#include <string>
#include <filesystem>
#include <url.hpp>
#include <fmt/format.h>
#include "common.h"
#include "gltf.h"
#include <stb_image_write.h>

class ImageUtil
{
public:
	ImageUtil(tinygltf::Model& model, const std::string& tempPath)
		: m_model(model)
		, m_temp(tempPath)
	{
		std::filesystem::path path(tempPath);
		std::filesystem::create_directories(path);
	}

	bool save(tinygltf::Image& image, std::string& outPath)
	{
		std::string name = image.name;
		if (!image.uri.empty())
		{
			Url url(image.uri);
			std::filesystem::path path(url.path());

			if (std::filesystem::exists(path))
			{
				// File exists!
				outPath = url.path();
				return true;
			}
			name = url.path();
		}

		if (name.empty())
		{
			static int roll = 0;
			name = "image_" + std::to_string(roll);
		}

		std::filesystem::path path(m_temp);
		path.append(name);

		if (!image.image.empty())
		{
			std::string cppstr = path.string();
			stbi_write_png(cppstr.c_str(), image.width, image.height, image.component, image.image.data(), 0);

			outPath = path.string();
			return true;
		}

		if (image.bufferView == -1)
		{
			return false;
		}

		tinygltf::BufferView& view = m_model.bufferViews[image.bufferView];
		if (view.buffer < 0)
		{
			return false;
		}

		tinygltf::Buffer& buffer = m_model.buffers[view.buffer];
		unsigned char* data = buffer.data.data();
		data += view.byteOffset;

		std::string cppstr = path.string();
		stbi_write_png(cppstr.c_str(), image.width, image.height, image.component, data, view.byteStride);

		outPath = path.string();
		return true;
	}

	bool compressBasis(const std::string& imagePath, std::string& outPath)
	{
		std::string command = fmt::format("basisu -file {0} -output_path {1}", imagePath, m_temp.string());

		std::string output;
		if (!runCommand(command, output))
		{
			return false;;
		}
		auto last = imagePath.find_last_of('.');
		outPath = fmt::format("{0}.basis", imagePath.substr(0, last));
		return true;
	}

	bool replace(tinygltf::Image& image, const std::string& imagePath)
	{
		if (imagePath.empty())
		{
			return false;
		}
		std::filesystem::path path{ imagePath };
		if (!std::filesystem::exists(path))
		{
			return false;
		}

		int viewIndex = 0;
		{
			tinygltf::BufferView view;
			{
				tinygltf::Buffer buffer;
				{
					size_t fileSize = std::filesystem::file_size(path);
					buffer.data.resize(fileSize);
					std::ifstream file(path, std::ios::binary);

					file.read((char*)&buffer.data[0], fileSize);
				}

				view.byteLength = buffer.data.size();
				view.buffer = m_model.buffers.size();
				m_model.buffers.push_back(std::move(buffer));
			}
			viewIndex = m_model.bufferViews.size();
			m_model.bufferViews.push_back(std::move(view));
		}

		auto extras = tinygltf::Value::Object();
		extras["basis"] = tinygltf::Value(tinygltf::Value::Object({
				{"bufferView", tinygltf::Value{viewIndex}}
			}));
		image.extras = tinygltf::Value(extras);

		return true;
	}

	bool compress(tinygltf::Image& image)
	{
		std::string diskImage;
		if (!save(image, diskImage))
		{
			return false;
		}
		std::string compressedImage;
		if (!compressBasis(diskImage, compressedImage))
		{
			return false;
		}
		return replace(image, compressedImage);
	}

	void packTextures()
	{
		std::vector<std::pair<size_t, std::string>> paths;
		paths.reserve(m_model.images.size());

		for (size_t i = 0; i < m_model.images.size(); ++i)
		{
			std::string out;
			if (save(m_model.images[i], out))
			{
				paths.push_back({ i, out });
			}
		}

		std::vector<std::pair<size_t, std::string>> compressed;
		compressed.reserve(m_model.images.size());
		for (const auto& pathPair : paths)
		{
			std::string out;
			if (compressBasis(pathPair.second, out))
			{
				compressed.push_back({ pathPair.first, out });
			}
		}

		for (const auto& pathPair : compressed)
		{
			if (replace(m_model.images[pathPair.first], pathPair.second))
			{
				continue;
			}
		}
	}
private:
	tinygltf::Model& m_model;
	std::filesystem::path m_temp;
};

#endif // GLTFFLUX_IMAGEUTIL_H_