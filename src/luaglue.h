#ifndef GLTFFLUX_LUAGLUE_H_
#define GLTFFLUX_LUAGLUE_H_

#include <string>
#include "common.h"
#include "imageutil.h"
#include "modelutil.h"
#include "gltf.h"
#include "scripting.h"

void setupGltf(Scripting& scripting)
{
	auto model = scripting.bind<tinygltf::Model, tinygltf::Model()>("Model");
	auto image = scripting.bind<tinygltf::Image, tinygltf::Image()>("Image");
}

void setupModelUtil(Scripting& scripting)
{
	auto modelutil = scripting.bind<ModelUtil, ModelUtil()>("ModelUtil");

	modelutil["load"] = &ModelUtil::load;
	modelutil["save"] = &ModelUtil::save;
	modelutil["getImage"] = &ModelUtil::getImage;
	modelutil["getImageCount"] = &ModelUtil::getImageCount;
}

void setupImageUtil(Scripting& scripting)
{
	auto imageutil = scripting.bind<ImageUtil, ImageUtil(tinygltf::Model&, const std::string&)>("ImageUtil");

	imageutil["save"] = &ImageUtil::save;
	imageutil["replace"] = &ImageUtil::replace;
	imageutil["compressBasis"] = &ImageUtil::compressBasis;
	imageutil["compress"] = &ImageUtil::compress;
	imageutil["packTextures"] = &ImageUtil::packTextures;
}

void setupScripting(Scripting& scripting)
{
	setupGltf(scripting);
	setupModelUtil(scripting);
	setupImageUtil(scripting);
}

#endif // GLTFFLUX_LUAGLUE_H_