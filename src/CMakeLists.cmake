cmake_minimum_required(VERSION 3.15)
project(gltfflux C CXX)

##############################################
# Configure
list(APPEND CMAKE_MODULE_PATH 
	${CMAKE_CURRENT_SOURCE_DIR}/cmake
)

##############################################
# Declare dependencies

# Project stuffs
add_executable(${PROJECT_NAME})

set(CMAKE_MODULE_PATH 
	${CMAKE_MODULE_PATH} 
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake"
)

file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/*.c
	${CMAKE_CURRENT_LIST_DIR}/*.inl
	${CMAKE_CURRENT_LIST_DIR}/*.h
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_SOURCES}
	)

target_include_directories(
	${PROJECT_NAME} 
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}"
)

target_link_libraries(
    ${PROJECT_NAME} 
	PRIVATE
		tinygltf
		CxxUrl
		fmt::fmt-header-only
		lua
		sol2
		basisulib
)

add_dependencies(${PROJECT_NAME} basis_universal_copy_binaries)

add_custom_command(
	TARGET ${PROJECT_NAME} 
	POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_CURRENT_LIST_DIR}/resources" "${PROJECT_BINARY_DIR}/resources")

## features
target_compile_features(
	${PROJECT_NAME} 
	PRIVATE cxx_std_17
)

## Add system information
cmake_host_system_information(RESULT build_hostname QUERY HOSTNAME)
target_compile_definitions(
	${PROJECT_NAME} 
	PRIVATE 
		BUILD_SYSTEM_HOSTNAME=\"${build_hostname}\"
		BUILD_SYSTEM_VERSION=\"${CMAKE_SYSTEM_VERSION}\"
		BUILD_SYSTEM_NAME=\"${CMAKE_SYSTEM_NAME}\"
		BUILD_PROJECT_NAME=\"${PROJECT_NAME}\"
)

## Add GIT information
find_package(Git)
if(GIT_FOUND)
	execute_process(
		COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
			WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
		OUTPUT_VARIABLE GIT_HASH
			OUTPUT_STRIP_TRAILING_WHITESPACE
		)
	target_compile_definitions(${PROJECT_NAME} PRIVATE GIT_HASH=\"${GIT_HASH}\")
else()
	target_compile_definitions(${PROJECT_NAME} PRIVATE GIT_HASH=\"ERROR\")
	message(STATUS "GIT not found")
endif()
