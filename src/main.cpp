#include <iostream>

#include "scripting.h"
#include "luaglue.h"

class Foo
{
public:
	Foo()
	{
	}

	int doo()
	{
		return 3;
	}
};

int main(void)
{
	Scripting scripting("resources");
	setupScripting(scripting);

	scripting.runFile("resources/main.lua");
	return 0;
}