#ifndef GLTFFLUX_COMMON_H_
#define GLTFFLUX_COMMON_H_

#include <string>
#include <cstdio>
#include <memory>

bool runCommand(const std::string& command, std::string& output)
{
#ifdef WIN32
	std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(command.c_str(), "r"), _pclose);
#else
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(command.c_str(), "r"), pclose);
#endif
	output.clear();
    const size_t bufferSize = 128;
    char buffer[128];
	if (!pipe)
	{
		return false;
	}
	while (fgets(buffer, bufferSize, pipe.get()) != nullptr)
	{
		output += buffer;
	}
	return true;
}

#endif // GLTFFLUX_COMMON_H_