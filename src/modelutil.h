#ifndef GLTFFLUX_MODELUTIL_H_
#define GLTFFLUX_MODELUTIL_H_

#include <string>
#include "common.h"
#include "gltf.h"

class ModelUtil {
public:
	ModelUtil()
	{
	}

	size_t getImageCount(tinygltf::Model& model) const
	{
		return model.images.size();
	}

	tinygltf::Image& getImage(tinygltf::Model& model, size_t index) const
	{
		return model.images[index];
	}

	bool load(tinygltf::Model& model, const std::string& filename) const
	{
		tinygltf::TinyGLTF loader;
		std::string err;
		std::string warn;

		bool fail = false;
		if (filename.back() == 'f')
		{
			fail = loader.LoadASCIIFromFile(&model, &err, &warn, filename);
		}
		else
		{
			fail = loader.LoadBinaryFromFile(&model, &err, &warn, filename);
		}

		return fail;
	}

	bool save(tinygltf::Model& model, const std::string& filename) const
	{
		tinygltf::TinyGLTF loader;
		return loader.WriteGltfSceneToFile(&model, filename, true, true, false, true);
	}
};

#endif // GLTFFLUX_MODELUTIL_H_