#ifndef GLTFFLUX_SCRIPTING_LUA_H_
#define GLTFFLUX_SCRIPTING_LUA_H_

#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <vector>
#include <utility>
#include <url.hpp>
#include <fmt/format.h>
#include "common.h"

#include <sol/sol.hpp>

class Scripting
{
public:
	Scripting(const std::string& includePath)
		: m_includePath(includePath)
	{
		m_lua.open_libraries(sol::lib::base);
	}

	bool run(const std::string& script)
	{
		m_lua.script(script);
		return true;
	}

	bool runFile(const std::string& path)
	{
		m_lua.script_file(path);
		return true;
	}

	template <class CType, typename... Args>
	sol::usertype<CType> bind(const std::string& className)
	{
		return m_lua.new_usertype<CType>(className, sol::constructors<Args...>());
	}

	sol::state& getState()
	{
		return m_lua;
	}
private:
	sol::state m_lua;

	std::string m_includePath;
public:
	void print(const std::string& text)
	{
		std::cout << text << std::endl;
	}
};

#endif // GLTFFLUX_SCRIPTING_LUA_H_