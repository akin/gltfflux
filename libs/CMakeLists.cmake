
set(FMT_TEST OFF)
set(FMT_INSTALL OFF)
set(FMT_DOC OFF)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/fmt)
set_target_properties(fmt PROPERTIES FOLDER "libs")

include(${CMAKE_CURRENT_LIST_DIR}/basis_universal.cmake)
set_target_properties(basisu PROPERTIES FOLDER "libs")

include(${CMAKE_CURRENT_LIST_DIR}/tinygltf.cmake)
set_target_properties(tinygltf PROPERTIES FOLDER "libs")

include(${CMAKE_CURRENT_LIST_DIR}/lua.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/sol2.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/cxxurl.cmake)
