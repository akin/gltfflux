#file(GLOB STB_SOURCES ${CMAKE_CURRENT_LIST_DIR}/*.h)
#add_library(stb STATIC ${STB_SOURCES})
add_library(tinygltf STATIC "${CMAKE_CURRENT_LIST_DIR}/empty.c")
target_include_directories(tinygltf INTERFACE ${CMAKE_CURRENT_LIST_DIR}/tinygltf)
