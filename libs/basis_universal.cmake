# this software is not written to be used as a "library", but as a binary.
# so my only option is this cmake hacky hacky ugly thing

# custom command, copy basis binary to build folder
# make it as dependency build step.

set(basisFolder ${CMAKE_CURRENT_LIST_DIR}/basis_universal)
add_subdirectory(${basisFolder})

set(BINARY_EXECUTABLE basisu)

if (WIN32)
    # WINDOWS
    set(BINARY_EXECUTABLE basisu.exe)
endif()

set(basisuBuildOutput ${PROJECT_BINARY_DIR}/${BINARY_EXECUTABLE})
add_custom_command(
    OUTPUT ${basisuBuildOutput}
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
            "$<$<CONFIG:Release>:${basisFolder}/bin/Release/${BINARY_EXECUTABLE}>" 
            "$<$<CONFIG:Debug>:${basisFolder}/bin/Debug/${BINARY_EXECUTABLE}>"
            ${PROJECT_BINARY_DIR}
    DEPENDS 
    basisu)

add_custom_target(
    basis_universal_copy_binaries
    DEPENDS 
        ${basisuBuildOutput}
)

###################
## basisulib implementation
project(basisulib)

cmake_minimum_required(VERSION 3.15)
cmake_policy(VERSION 3.15)

set(BASISU_SRC_LIST 
    ${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_backend.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_basis_file.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_comp.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_enc.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_etc.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_frontend.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_global_selector_palette_helpers.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_gpu_texture.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_pvrtc1_4.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_resampler.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_resample_filters.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_ssim.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_astc_decomp.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_uastc_enc.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/basisu_bc7enc.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/lodepng.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/apg_bmp.c
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/jpgd.cpp
	${CMAKE_CURRENT_LIST_DIR}/basis_universal/transcoder/basisu_transcoder.cpp
	)

add_library(${PROJECT_NAME} STATIC)

target_sources(
    ${PROJECT_NAME} 
	PRIVATE 
		${BASISU_SRC_LIST}
)

target_compile_definitions(
	${PROJECT_NAME} 
	PRIVATE 
        MAKE_LIB
)

target_include_directories(
	${PROJECT_NAME} 
	PUBLIC
	    ${CMAKE_CURRENT_LIST_DIR}/basis_universal
)
