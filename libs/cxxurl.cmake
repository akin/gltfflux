cmake_minimum_required(VERSION 3.15)
project(CxxUrl C CXX)

set(CMAKE_CXX_STANDARD 14)

set(HEADERS
	${CMAKE_CURRENT_LIST_DIR}/CxxUrl/url.hpp
	${CMAKE_CURRENT_LIST_DIR}/CxxUrl/string.hpp)

set(SOURCES
	${HEADERS}
	${CMAKE_CURRENT_LIST_DIR}/CxxUrl/url.cpp)

add_library(${PROJECT_NAME} STATIC)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${SOURCES}
)

target_include_directories(
	${PROJECT_NAME} 
	PUBLIC
		${CMAKE_CURRENT_LIST_DIR}/CxxUrl
)
