cmake_minimum_required(VERSION 3.15)

project(lua C)

##############################################
# Configure

##############################################
# Declare dependencies

##############################################
# Create target and set properties
add_library(${PROJECT_NAME} STATIC)

file(GLOB CURRENT_PUBLICS
	${CMAKE_CURRENT_LIST_DIR}/lua/*.h
)

file(GLOB CURRENT_PRIVATES
	${CMAKE_CURRENT_LIST_DIR}/lua/*.c
)

function(removeFile filename listname)
    get_filename_component(full_path ${filename} ABSOLUTE)
    set(arrr ${ARGN})
    list(REMOVE_ITEM arrr ${full_path})
    set(${listname} ${arrr} PARENT_SCOPE)
endfunction()

removeFile(${CMAKE_CURRENT_LIST_DIR}/lua/lua.c CURRENT_PRIVATES ${CURRENT_PRIVATES})
removeFile(${CMAKE_CURRENT_LIST_DIR}/lua/luac.c CURRENT_PRIVATES ${CURRENT_PRIVATES})

target_sources(
    ${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_PUBLICS}
		${CURRENT_PRIVATES}
)

target_compile_definitions(
	${PROJECT_NAME} 
	PRIVATE 
        MAKE_LIB
)

target_include_directories(
	${PROJECT_NAME} 
	PUBLIC
	    ${CMAKE_CURRENT_LIST_DIR}/lua
)

##############################################
# Installation

##############################################
# Exportins

##############################################
# Testing & Dev