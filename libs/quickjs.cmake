cmake_minimum_required(VERSION 3.15)

project(quickjs C)

##############################################
# Configure

##############################################
# Declare dependencies

##############################################
# Create target and set properties
add_library(${PROJECT_NAME} STATIC)

file(GLOB CURRENT_PUBLICS
	${CMAKE_CURRENT_LIST_DIR}/quickjs/*.h
)

file(GLOB CURRENT_PRIVATES
	${CMAKE_CURRENT_LIST_DIR}/private/*.h
	${CMAKE_CURRENT_LIST_DIR}/quickjs/*.c
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_PUBLICS}
		${CURRENT_PRIVATES}
)

target_include_directories(
	${PROJECT_NAME} 
	PUBLIC
	    ${CMAKE_CURRENT_LIST_DIR}/quickjs
)

##############################################
# Installation

##############################################
# Exportins

##############################################
# Testing & Dev