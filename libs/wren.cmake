cmake_minimum_required(VERSION 3.15)

project(wren C)

##############################################
# Configure

##############################################
# Declare dependencies

##############################################
# Create target and set properties
add_library(${PROJECT_NAME} STATIC)

file(GLOB CURRENT_PUBLICS
	${CMAKE_CURRENT_LIST_DIR}/wren/src/include/*.h
	${CMAKE_CURRENT_LIST_DIR}/wren/src/include/*.hpp
	${CMAKE_CURRENT_LIST_DIR}/wren/src/optional/*.h
	${CMAKE_CURRENT_LIST_DIR}/wren/src/optional/*.hpp
	${CMAKE_CURRENT_LIST_DIR}/wren/src/vm/*.h
	${CMAKE_CURRENT_LIST_DIR}/wren/src/vm/*.hpp
)

file(GLOB CURRENT_PRIVATES
	${CMAKE_CURRENT_LIST_DIR}/wren/src/vm/*.c
	${CMAKE_CURRENT_LIST_DIR}/wren/src/optional/*.c
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_PUBLICS}
		${CURRENT_PRIVATES}
)

target_include_directories(
	${PROJECT_NAME} 
	PUBLIC
		${CMAKE_CURRENT_LIST_DIR}/wren/src/include
		${CMAKE_CURRENT_LIST_DIR}/wren/src/optional
		${CMAKE_CURRENT_LIST_DIR}/wren/src/vm
)

##############################################
# Installation

##############################################
# Exportins

##############################################
# Testing & Dev